package listeners;

import model.beans.User;
import model.util.DBHelper;

import javax.servlet.*;
import javax.servlet.annotation.WebListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebListener
public class MainListener implements ServletContextListener {

    private static int USER_NUMBER = 0;
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();

        DBHelper help = new DBHelper();

        System.out.println("Application info: " + context.getServerInfo());
        System.out.println("Servlet context name: " + context.getServletContextName());
        System.out.println("Available servlets: " + context.getServletRegistrations().keySet());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("Context destroyed");
    }
}
