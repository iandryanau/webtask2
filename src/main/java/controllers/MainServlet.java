package controllers;

import model.beans.User;
import model.util.DBHelper;

import javax.servlet.ServletContext;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "MainServlet", value = "/home")
public class MainServlet extends javax.servlet.http.HttpServlet {

    private static final String ATTRIBUTE_USERS = "users";
    private boolean isAvailable;

    @Override
    public void init() {
        isAvailable = !Boolean.parseBoolean(getServletContext().getInitParameter("isAvailable"));
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        ServletContext context = getServletContext();
        synchronized (context) {
            if (!isAvailable) {
                isAvailable = true;
                throw new UnavailableException("Something's wrong",
                        Integer.parseInt(context.getInitParameter("errorTime")));
            }
        }
        List<User> users = null;
        try {
            users = new DBHelper().getAllUsers();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Session creation time: " + request.getSession().getCreationTime());
        System.out.println("Last accessed time: " + request.getSession().getLastAccessedTime());

        request.setAttribute(ATTRIBUTE_USERS, users);
        System.out.println(request.getRequestURL());
        request.getRequestDispatcher("users.jsp").forward(request, response);
    }
}
