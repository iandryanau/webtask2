package controllers;

import model.beans.User;
import model.util.DBHelper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UserPage", value = "/userPage")
public class UserPage extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println(request.getRequestURL());
        request.getRequestDispatcher("userPage.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("name");
        String lastName = req.getParameter("lastName");

        DBHelper helper = new DBHelper();

        helper.updateUser(firstName, lastName, ((User) req.getSession().getAttribute("userId")).getLogin());

        ((User) req.getSession().getAttribute("userId")).setFirstName(firstName);
        ((User) req.getSession().getAttribute("userId")).setLastName(lastName);

        doGet(req, resp);
    }
}
