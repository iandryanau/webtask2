package model.util;

import model.beans.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBHelper {

    private final static String GET_ALL_USERS = "SELECT * FROM userslist";
    private final static String ADD_USER = "INSERT INTO userslist(name) VALUES (?,?)";
    private final static String REGISTER_ACCOUNT = "INSERT INTO users(firstName, lastName, login, password) VALUES (?,?,?,?)";
    private final static String FIND_USER = "SELECT * FROM users WHERE login = ? AND password = ?";
    private final static String UPDATE_USER = "UPDATE users SET firstName = ?, lastName = ? WHERE login = ?";

    private final static String CLASS_PATH = "com.mysql.jdbc.Driver";
    private final static String DB_CONNECTION = "jdbc:mysql://localhost:3306/usersdatabase";

    public DBHelper() {
        //empty
    }

    private Connection getConnect() throws ClassNotFoundException, SQLException {
        Class.forName(CLASS_PATH);
        return DriverManager.getConnection(DB_CONNECTION,
                "root", "");
    }

    public List<User> getAllUsers() throws SQLException, ClassNotFoundException {
        Connection connect;
        Statement statement;
        ResultSet resultSet;

        List<User> users = new ArrayList<>();
        User user;
        connect = getConnect();
        statement = connect.createStatement();
        resultSet = statement.executeQuery(GET_ALL_USERS);

        while (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getInt(1));
            user.setFirstName(resultSet.getString(2));
            users.add(user);
        }

        return users;
    }

    public void addUsers(List<User> users) throws SQLException {
        Connection connect = null;
        PreparedStatement preparedStatement;
        try {
            connect = getConnect();
            connect.setAutoCommit(false);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        try {
            preparedStatement = connect.prepareStatement(ADD_USER);

            for (User user : users) {
                preparedStatement.setString(1, user.getFirstName());
                preparedStatement.executeUpdate();
            }
            connect.commit();
        } catch (SQLException e) {
            System.out.println("SQLException. Executing rollback to savepoint...");
            connect.rollback();
        }
    }

    public void addNewAccount(User user) {
        Connection connect = null;
        PreparedStatement preparedStatement;
        try {
            connect = getConnect();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        try {
            preparedStatement = connect.prepareStatement(REGISTER_ACCOUNT);
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public User findUser(String login, String password) {
        Connection connect;
        ResultSet resultSet;
        PreparedStatement preparedStatement;
        User user = new User();
        try {
            connect = getConnect();
            preparedStatement = connect.prepareStatement(FIND_USER);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setFirstName(resultSet.getString("firstName"));
                user.setLastName(resultSet.getString("lastName"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    public void updateUser(String firstName, String lastName, String login){
        Connection connect;
        ResultSet resultSet;
        PreparedStatement preparedStatement;
        User user = new User();

        try {
            connect = getConnect();
            preparedStatement = connect.prepareStatement(UPDATE_USER);
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            preparedStatement.setString(3,login);

            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }
}
