<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<form method="post" action="/registration">
    <table border="0" width="30%" cellpadding="3">
        <thead>
        <tr>
            <th colspan="2">Registration form</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>First Name:</td>
            <td><input type="text" name="firstName"></td>
        </tr>
        <tr>
            <td>Last Name:</td>
            <td><input type="text" name="lastName"></td>
        </tr>
        <tr>
            <td>Login:</td>
            <td><input type="text" name="login"></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td><input type="submit" value="Sing in"/></td>
            <td><input type="reset" value="Reset"/></td>
        </tr>
        <tr>
            <td colspan="2"><a href="index.jsp">Back to main page</a></td>
        </tr>
        </tbody>
    </table>

</form>
</body>
</html>
