<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Account Information</title>
</head>
<body>

<table border="0" width="30%" cellpadding="3">
    <thead>
    <tr>
        <th colspan="2">Account information</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Name:</td>
        <td>${userId.firstName}</td>
    </tr>
    <tr>
        <td>Last name:</td>
        <td>${userId.lastName}</td>
    </tr>
    </tbody>
</table>

<hr/>

<form method="post" action="/userPage">
    <table border="0" width="30%" cellpadding="3">
        <tbody>
        <tr>
            <td>Name:</td>
            <td><input type="text" name="name" value="${userId.firstName}"/></td>
        </tr>
        <tr>
            <td>LastName:</td>
            <td><input type="text" name="lastName", value="${userId.lastName}"/></td>
        </tr>
        <tr>
            <td><input type="submit" value="Submit"/></td>
            <td><input type="reset" value="Reset"/></td>
        </tr>
        </tbody>
    </table>
</form>

<a href="/">Back to main page</a>
</body>
</html>
