<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Authorization</title>
</head>
<body>
<form method="post" action="/login">
    <table border="0" width="30%" cellpadding="3">
        <thead>
        <tr>
            <th colspan="2">Login form</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Login:</td>
            <td><input type="text" name="login" value=""/></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type="password" name="password" value=""/></td>
        </tr>
        <tr>
            <td><input type="submit" value="Login"/></td>
            <td><input type="reset" value="Reset"/></td>
        </tr>
        <tr>
            <td><%=(request.getAttribute("errorMessage") == null) ? ""
                    : request.getAttribute("errorMessage")%>
            </td>
        </tr>
        <tr>
            <td colspan="2">If you not registered<a href="registration.jsp">Register Here</a></td>
        </tr>
        </tbody>
    </table>
    <a href="index.jsp">Back to main page</a>
</form>
</body>
</html>
