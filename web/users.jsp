<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
<table>
    <c:forEach items="${users}" var="user">
        <tr>
            <td>
                    ${user}
            </td>
        </tr>
    </c:forEach>
</table>
<a href="/">Back to main page</a></td>
</body>
</html>
